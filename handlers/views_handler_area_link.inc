<?php

/**
* @file
* Views area link handler.
*/

/**
 * @ingroup views_area_handlers Views' area handlers
*/

class views_handler_area_link extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();
    
    $options += array(
      'text' => array('default' => '', 'translatable' => TRUE),
      'path' => array('default' => '', 'translatable' => TRUE),
      'absolute' => array('default' => '', 'translatable' => FALSE),
      'external' => array('default' => '', 'translatable' => FALSE),
      'replace_spaces' => array('default' => '', 'translatable' => FALSE),
      'trim_whitespace' => array('default' => FALSE),
      'alt' => array('default' => '', 'translatable' => TRUE),
      'rel' => array('default' => ''),
      'link_class' => array('default' => ''),
      'prefix' => array('default' => '', 'translatable' => TRUE),
      'suffix' => array('default' => '', 'translatable' => TRUE),
      'target' => array('default' => '', 'translatable' => TRUE),
      'nl2br' => array('default' => FALSE),
      'max_length' => array('default' => ''),
      'word_boundary' => array('default' => TRUE),
      'ellipsis' => array('default' => TRUE),
      'strip_tags' => array('default' => FALSE),
      'trim' => array('default' => FALSE),
      'preserve_tags' => array('default' => ''),
      'html' => array('default' => FALSE),
    );
    
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['text'] = array(
      '#title' => t('Link text'),
      '#type' => 'textfield',
      '#default_value' => $this->options['text'],
      '#description' => t('The text for this link. You may enter data from this view as per the "Replacement patterns" below.'),
      '#maxlength' => 255,
    );
    $form['path'] = array(
      '#title' => t('Link path'),
      '#type' => 'textfield',
      '#default_value' => $this->options['path'],
      '#description' => t('The Drupal path or absolute URL for this link. You may enter data from this view as per the "Replacement patterns" below.'),
      '#maxlength' => 255,
    );
    $form['absolute'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use absolute path'),
      '#default_value' => $this->options['absolute'],
    );
    $form['replace_spaces'] = array(
      '#type' => 'checkbox',
      '#title' => t('Replace spaces with dashes'),
      '#default_value' => $this->options['replace_spaces'],
    );
    $form['external'] = array(
      '#type' => 'checkbox',
      '#title' => t('External server URL'),
      '#default_value' => $this->options['external'],
      '#description' => t("Links to an external server using a full URL: e.g. 'http://www.example.com' or 'www.example.com'."),
    );
    $form['link_class'] = array(
      '#title' => t('Link class'),
      '#type' => 'textfield',
      '#default_value' => $this->options['link_class'],
      '#description' => t('The CSS class to apply to the link.'),
    );
    $form['alt'] = array(
      '#title' => t('Alt text'),
      '#type' => 'textfield',
      '#default_value' => $this->options['alt'],
      '#description' => t('Text to place as "alt" text which most browsers display as a tooltip when hovering over the link.'),
    );
    $form['rel'] = array(
      '#title' => t('Rel Text'),
      '#type' => 'textfield',
      '#default_value' => $this->options['rel'],
      '#description' => t('Include Rel attribute for use in lightbox2 or other javascript utility.'),
    );
    $form['prefix'] = array(
      '#title' => t('Prefix text'),
      '#type' => 'textfield',
      '#default_value' => $this->options['prefix'],
      '#description' => t('Any text to display before this link. You may include HTML.'),
    );
    $form['suffix'] = array(
      '#title' => t('Suffix text'),
      '#type' => 'textfield',
      '#default_value' => $this->options['suffix'],
      '#description' => t('Any text to display after this link. You may include HTML.'),
    );
    $form['target'] = array(
      '#title' => t('Target'),
      '#type' => 'textfield',
      '#default_value' => $this->options['target'],
      '#description' => t("Target of the link, such as _blank, _parent or an iframe's name. This field is rarely used."),
    );

    // Get a list of the available fields and arguments for token replacement.
    $options = array();
    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      $options[t('Fields')]["[$field]"] = $handler->ui_name();
      // We only use fields up to (and including) this one.
      if ($field == $this->options['id']) {
        break;
      }
    }
    $count = 0; // This lets us prepare the key as we want it printed.
    foreach ($this->view->display_handler->get_handlers('argument') as $arg => $handler) {
      $options[t('Arguments')]['%' . ++$count] = t('@argument title', array('@argument' => $handler->ui_name()));
      $options[t('Arguments')]['!' . $count] = t('@argument input', array('@argument' => $handler->ui_name()));
    }

    // Default text.
    $output = t('<p>You must add some additional fields to this display before using this field. These fields may be marked as <em>Exclude from display</em> if you prefer. Note that due to rendering order, you cannot use fields that come after this field; if you need a field not listed here, rearrange your fields.</p>');
    // We have some options, so make a list.
    if (!empty($options)) {
      $output = t('<p>The following tokens are available for this field. Note that due to rendering order, you cannot use fields that come after this field; if you need a field not listed here, rearrange your fields.
If you would like to have the characters %5B and %5D please use the html entity codes \'%5B\' or  \'%5D\' or they will get replaced with empty space.</p>');
      foreach (array_keys($options) as $type) {
        if (!empty($options[$type])) {
          $items = array();
          foreach ($options[$type] as $key => $value) {
            $items[] = $key . ' == ' . $value;
          }
          $output .= theme('item_list',
            array(
              'items' => $items,
              'type' => $type
            ));
        }
      }
    }

    // This construct uses 'hidden' and not markup because process doesn't
    // run. It also has an extra div because the dependency wants to hide
    // the parent in situations like this, so we need a second div to
    // make this work.
    $form['help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacement patterns'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => $output,
      '#dependency' => array(
        'edit-options-alter-alter-text' => array(1),
      ),
    );
  }
  /*
  function options_submit(&$form, &$form_state) {
    $form_state['values']['options']['format'] = $form_state['values']['options']['content']['format'];
    $form_state['values']['options']['content'] = $form_state['values']['options']['content']['value'];
    parent::options_submit($form, $form_state);
  }
  */
  function render($empty = FALSE) {
    $tokens = $this->get_render_tokens($this->options);

    return $this->render_as_link($this->options, strtr($this->options['text'], $tokens), $tokens);
  }

 /**
   * Render this field as a link, with the info from a fieldset set by
   * the user.
   */
  function render_as_link($alter, $text, $tokens) {
    $value = '';

    if (!empty($alter['prefix'])) {
      $value .= filter_xss_admin(strtr($alter['prefix'], $tokens));
    }

    $options = array(
      'html' => TRUE,
      'absolute' => !empty($alter['absolute']) ? TRUE : FALSE,
    );

    // $path will be run through check_url() by l() so we do not need to
    // sanitize it ourselves.
    $path = $alter['path'];

    // html_entity_decode removes <front>, so check whether its different to front.
    if ($path != '<front>') {
      // Use strip tags as there should never be HTML in the path.
      // However, we need to preserve special characters like " that
      // were removed by check_plain().
      $path = strip_tags(html_entity_decode(strtr($path, $tokens)));

      if (!empty($alter['replace_spaces'])) {
        $path = str_replace(' ', '-', $path);
      }
    }

    // Parse the URL and move any query and fragment parameters out of the path.
    $url = parse_url($path);

    // Seriously malformed URLs may return FALSE or empty arrays.
    if (empty($url)) {
      return $text;
    }

    // If the path is empty do not build a link around the given text and return
    // it as is.
    // http://www.example.com URLs will not have a $url['path'], so check host as well.
    if (empty($url['path']) && empty($url['host'])) {
      return $text;
    }

    // If no scheme is provided in the $path, assign the default 'http://'.
    // This allows a url of 'www.example.com' to be converted to 'http://www.example.com'.
    // Only do this on for external URLs.
    if ($alter['external']){
      if (!isset($url['scheme'])) {
        // There is no scheme, add the default 'http://' to the $path.
        $path = "http://$path";
        // Reset the $url array to include the new scheme.
        $url = parse_url($path);
      }
    }

    if (isset($url['query'])) {
      $path = strtr($path, array('?' . $url['query'] => ''));
      $options['query'] = drupal_get_query_array($url['query']);
    }
    if (isset($url['fragment'])) {
      $path = strtr($path, array('#' . $url['fragment'] => ''));
      // If the path is empty we want to have a fragment for the current site.
      if ($path == '') {
        $options['external'] = TRUE;
      }
      $options['fragment'] = $url['fragment'];
    }

    $alt = strtr($alter['alt'], $tokens);
    // Set the title attribute of the link only if it improves accessibility
    if ($alt && $alt != $text) {
      $options['attributes']['title'] = html_entity_decode($alt, ENT_QUOTES);
    }

    $class = strtr($alter['link_class'], $tokens);
    if ($class) {
      $options['attributes']['class'] = array($class);
    }

    if (!empty($alter['rel']) && $rel = strtr($alter['rel'], $tokens)) {
      $options['attributes']['rel'] = $rel;
    }

    $target = check_plain(trim(strtr($alter['target'],$tokens)));
    if (!empty($target)) {
      $options['attributes']['target'] = $target;
    }

    // Allow the addition of arbitrary attributes to links. Additional attributes
    // currently can only be altered in preprocessors and not within the UI.
    if (isset($alter['link_attributes']) && is_array($alter['link_attributes'])) {
      foreach ($alter['link_attributes'] as $key => $attribute) {
        if (!isset($options['attributes'][$key])) {
          $options['attributes'][$key] = strtr($attribute, $tokens);
        }
      }
    }

    // If the query and fragment were programatically assigned overwrite any
    // parsed values.
    if (isset($alter['query'])) {
      // Convert the query to a string, perform token replacement, and then
      // convert back to an array form for l().
      $options['query'] = drupal_http_build_query($alter['query']);
      $options['query'] = strtr($options['query'], $tokens);
      $options['query'] = drupal_get_query_array($options['query']);
    }
    if (isset($alter['alias'])) {
      // Alias is a boolean field, so no token.
      $options['alias'] = $alter['alias'];
    }
    if (isset($alter['fragment'])) {
      $options['fragment'] = strtr($alter['fragment'], $tokens);
    }
    if (isset($alter['language'])) {
      $options['language'] = $alter['language'];
    }

    // If the url came from entity_uri(), pass along the required options.
    if (isset($alter['entity'])) {
      $options['entity'] = $alter['entity'];
    }
    if (isset($alter['entity_type'])) {
      $options['entity_type'] = $alter['entity_type'];
    }

    $value .= l($text, $path, $options);

    if (!empty($alter['suffix'])) {
      $value .= filter_xss_admin(strtr($alter['suffix'], $tokens));
    }

    return $value;
  }

  /**
   * Get the 'render' tokens to use for advanced rendering.
   *
   * This runs through all of the fields and arguments that
   * are available and gets their values. This will then be
   * used in one giant str_replace().
   */
  function get_render_tokens($item) {
    $tokens = array();
    if (!empty($this->view->build_info['substitutions'])) {
      $tokens = $this->view->build_info['substitutions'];
    }
    $count = 0;
    foreach ($this->view->display_handler->get_handlers('argument') as $arg => $handler) {
      $token = '%' . ++$count;
      if (!isset($tokens[$token])) {
        $tokens[$token] = '';
      }

      // Use strip tags as there should never be HTML in the path.
      // However, we need to preserve special characters like " that
      // were removed by check_plain().
      $tokens['!' . $count] = isset($this->view->args[$count - 1]) ? strip_tags(html_entity_decode($this->view->args[$count - 1])) : '';
    }

    // Now add replacements for our fields.
    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      if (isset($handler->last_render)) {
        $tokens["[$field]"] = $handler->last_render;
      }
      else {
        $tokens["[$field]"] = '';
      }

      // We only use fields up to (and including) this one.
      if ($field == $this->options['id']) {
        break;
      }
    }

    // Store the tokens for the row so we can reference them later if necessary.
    // $this->view->style_plugin->render_tokens[$this->view->row_index] = $tokens;
    $this->last_tokens = $tokens;
    return $tokens;
  }

}

